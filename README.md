# React assignment exercise
You can see live preview [here](https://coolblue-fani.herokuapp.com)


## How to

### Run for first time

To start the app for the first time, you can run this command in terminal.

```yarn && yarn start```


If you already installed all the dependecies, you can simply run this command.

```yarn start```

### Test

To test the app, you can run this command in terminal.

```yarn test --coverage```

## Assignment

### Goal

The goal of this assignment is to convert the provided designs below to a working interface using React.

- [Homepage - Desktop](designs/Red%20Alert%20-%20Homepage%20-%20Desktop.png)
- [Homepage - Mobile](designs/Red%20Alert%20-%20Homepage%20-%20Mobile.png)
- [More Information Popup - Desktop](designs/Red%20Alert%20-%20More%20Information%20Popup%20-%20Desktop.png)
- [Q&A Expanded - Mobile](designs/Red%20Alert%20-%20Q&A%20Expanded%20-%20Mobile.png)

### Scope

- [*] Set up of the React project
- [*] Responsive design
- [*] Accessible
- [*] Being able to open and close the FAQ items
- [*] Only one FAQ item is allowed to be open
- [*] Being able to open and close the modal box
- [*] Functionalities are unit tested
- [*] Compatible with latest Chrome and Firefox versions 
- [*] Update this README with instructions on how to run this application

## Design specifics

- Colors used:
    - Dark grey: `#2B2D42`
    - Grey: `#8D99AE`
    - Light grey: `#EDF2F4`
    - Dark red: `#7F1028`
    - Light red: `#AD646D`
    - White: `#FFFFFF` 
- Font used is `Helvetica`
- Static assets
    - [Product image 1 png](./public/assets/images/product-1-transparent.png)
    - [Product image 1 webp](./public/assets/images/product-1-transparent.webp)
    - [Product image 2 png](./public/assets/images/product-2-transparent.png)
    - [Product image 2 webp](./public/assets/images/product-2-transparent.webp)

## Assets

The `designs` are added to the `./designs` directory.
All the `images` needed to implement the designs are added to the `./public/assets/images` directory.

## Questions

- Q: How much time do I have?
- A: Try to limit your time to 4 hours total.

If you have any other questions while working on the exercise feel free to reach out. We will be happy to help.

Happy coding!
