import React from 'react';
import { useDispatch } from 'react-redux';

import { actions } from '../../Redux/Reducer/Modal.reducer';
import FAQ from '../../Components/FAQ';
import Modal from '../../Components/Modal';
import Button from '../../Components/Button';
import './Home.screen.styles.scss';

const noop = () => {};

const _onInfoClick = (dispatch) => () => {
  dispatch(actions.setShow(true));
};

const _renderHeader = () => (
  <div className="header">
    <span data-type="logo-text">Red Alert</span>
    <span data-type="logo-subtext">by Warning</span>
  </div>
);

const _renderImage = (fileName, alt, isMobile) => (
  <div className="imageContainer" data-mobile={isMobile}>
    <picture>
      <source srcSet={`assets/images/${fileName}.webp`} type="image/webp" />
      <source srcSet={`assets/images/${fileName}.png`} type="image/png" />
      <img src={`assets/images/${fileName}.webp`} alt={alt} />
    </picture>
  </div>
);

const _renderBanner = (dispatch) => (
  <div className="banner">
    {_renderImage('product-2-transparent', 'Warning Red Alert Basic', false)}
    <div className="contentContainer">
      <h1 className="title">Protect your home</h1>
      <span className="subtitle">for a criminally low price</span>
      {_renderImage('product-1-transparent', 'Warning Red Alert Premium', true)}
      <div className="buttonContainer">
        <Button title="Get started for € 59,99" onClick={noop} variant="red" id="startButton" />
        <Button title="More Information" onClick={_onInfoClick(dispatch)} variant="black" id="infoButton" />
      </div>
    </div>
    {_renderImage('product-1-transparent', 'Warning Red Alert Premium', false)}
  </div>
);

const Home = () => {
  const dispatch = useDispatch();

  return (
    <>
      {_renderHeader()}
      {_renderBanner(dispatch)}
      <FAQ />
      <Modal />
    </>
  );
};

export default Home;
