import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { useDispatch, useSelector } from 'react-redux';

import { actions } from '../../Redux/Reducer/Modal.reducer';
import Home from './Home.screen';

jest
  .mock('react-redux', () => ({
    useSelector: jest.fn(),
    useDispatch: jest.fn()
  }))
  .mock('../../Redux/Reducer/Modal.reducer', () => ({
    actions: {
      setShow: jest.fn()
    }
  }));

jest.useFakeTimers();

describe('Home Screen test', () => {
  const state = {
    show: false
  };

  it('should return correct snapshot ', () => {
    useSelector.mockReturnValue(state);
    const { baseElement } = render(<Home />);

    expect(baseElement).toMatchSnapshot();
  });

  it('should dispatch set show to true when info button clicked', () => {
    const mockDispatch = jest.fn();
    useSelector.mockReturnValue(state);
    useDispatch.mockReturnValue(mockDispatch);
    const { getByTestId } = render(<Home />);
    const button = getByTestId('infoButton');

    fireEvent.click(button);

    expect(mockDispatch).toBeCalled();
    expect(actions.setShow).toHaveBeenCalledWith(true);
  });

  it('should do nothing when start button clicked', () => {
    const mockDispatch = jest.fn();
    useSelector.mockReturnValue(state);
    useDispatch.mockReturnValue(mockDispatch);
    const { getByTestId } = render(<Home />);
    const button = getByTestId('startButton');

    fireEvent.click(button);

    expect(mockDispatch).not.toBeCalled();
  });
});
