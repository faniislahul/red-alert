import { configureStore } from '@reduxjs/toolkit';

import modalReducer from './Reducer/Modal.reducer';

export default configureStore({
  reducer: {
    modal: modalReducer,
  },
});
