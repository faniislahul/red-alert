import { commonHandler } from './Modal.reducer';

describe('Common handler', () => {
  it('should change the state', () => {
    const initialState = {
      show: false,
    };
    const expectedResult = {
      ...initialState,
      show: true
    };

    const mockKey = 'show';

    commonHandler(mockKey)(initialState, { payload: true });

    expect(initialState).toStrictEqual(expectedResult);
  });
});
