import { createSlice } from '@reduxjs/toolkit';

// Reducers

/**
 * commonHandler -> common redux handler
 * @param {string} key - state object
 * @returns {Function} reducer handler
 */
export const commonHandler = (key) => (state, { payload }) => {
  state[key] = payload;
};

export const ClientSlice = createSlice({
  name: 'modal',
  initialState: {
    show: false,
  },
  reducers: {
    setShow: commonHandler('show'),
  },
});

export const { actions } = ClientSlice;

export default ClientSlice.reducer;
