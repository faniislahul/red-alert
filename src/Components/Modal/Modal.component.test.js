import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { useSelector, useDispatch } from 'react-redux';
import { actions } from '../../Redux/Reducer/Modal.reducer';

import Modal from './Modal.component';

jest
  .mock('react-redux', () => ({
    useSelector: jest.fn(),
    useDispatch: jest.fn()
  }))
  .mock('../../Redux/Reducer/Modal.reducer', () => ({
    actions: {
      setShow: jest.fn()
    }
  }));

jest.useFakeTimers();

describe('Modal Component test', () => {
  const state = {
    show: true
  };

  it('should return correct snapshot when modal is show', () => {
    useSelector.mockReturnValue(state);
    const { baseElement } = render(<Modal />);
    jest.runAllTimers();

    expect(baseElement).toMatchSnapshot();
  });

  it('should return correct snapshot when modal is not show', () => {
    const mockState = {
      show: false
    };
    useSelector.mockReturnValue(mockState);
    const { baseElement } = render(<Modal />);
    jest.runAllTimers();

    expect(baseElement).toMatchSnapshot();
  });

  it('should dispatch set show to false when close button clicked', () => {
    const mockDispatch = jest.fn();
    useSelector.mockReturnValue(state);
    useDispatch.mockReturnValue(mockDispatch);
    const { getByTestId } = render(<Modal />);
    const button = getByTestId('closeButton');

    fireEvent.click(button);

    expect(mockDispatch).toBeCalled();
    expect(actions.setShow).toHaveBeenCalledWith(false);
  });
});
