import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { actions } from '../../Redux/Reducer/Modal.reducer';
import Button from '../Button';
import './Modal.component.styles.scss';

const _onModalClose = (dispatch) => () => {
  dispatch(actions.setShow(false));
};

const _renderImage = (fileName, alt) => (
  <picture>
    <source srcSet={`assets/images/${fileName}.webp`} type="image/webp" />
    <source srcSet={`assets/images/${fileName}.png`} type="image/png" />
    <img src={`assets/images/${fileName}.webp`} alt={alt} />
  </picture>
);

const _renderItem = () => (
  <>
    <div className="productItem">
      {_renderImage('product-2-transparent', 'Warning Red Alert Basic')}
      <h3 className="productName">Warning Red Alert Basic</h3>
      <span className="productDescription">Contains 3 door sensors. 2 door locks. 1 alarm and 1 hub.</span>
      <br />
      <span className="productPrice">€ 59,99</span>
    </div>
    <div className="productItem">
      {_renderImage('product-1-transparent', 'Warning Red Alert Premium')}
      <h3 className="productName">Warning Red Alert Premium</h3>
      <span className="productDescription">Contains everything Red Alert Basic has, plus a weather proof camera</span>
      <br />
      <span className="productPrice">€ 109,99</span>
    </div>
  </>
);

const _renderHeader = () => (
  <div className="modalHeader">
    <span>More Information</span>
  </div>
);

const useAnimationEffect = (show, setDisplay, setAnimate) => {
  React.useEffect(() => {
    if (show) {
      setDisplay(true);
      setTimeout(() => {
        setAnimate(true);
        document.getElementById('closeButton').focus();
      }, 100);
    } else {
      setAnimate(false);
      setTimeout(() => { setDisplay(false); }, 300);
    }
  }, [show]);
};

const Modal = () => {
  const { show } = useSelector((state) => state.modal);
  const [display, setDisplay] = React.useState(false);
  const [animate, setAnimate] = React.useState(false);
  const dispatch = useDispatch();
  useAnimationEffect(show, setDisplay, setAnimate);

  return (
    <div
      className="modalBackdrop"
      data-display={display}
      data-show={animate}
      role="dialog"
      aria-hidden={!display}
      aria-modal
    >
      <div className="modalContainer">
        {_renderHeader()}
        <div className="modalBody">
          {_renderItem()}
          <Button title="Close" onClick={_onModalClose(dispatch)} variant="red" id="closeButton" />
        </div>
      </div>
    </div>
  );
};

export default Modal;
