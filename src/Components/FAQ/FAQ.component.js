import React from 'react';

import './FAQ.component.styles.scss';

const questionList = [
  {
    q: 'How long does Red Alert stay active after enabling?',
    a: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sagittis enim id consectetur commodo. Fusce ultricies mattis maximus. Aenean purus quam, interdum at semper sed, efficitur nec risus. Fusce risus enim, pulvinar eu vestibulum id, semper nec libero. Curabitur condimentum sem vel euismod molestie.'
  },
  {
    q: 'When should I replace Red Alert?',
    a: 'Etiam rutrum massa et tortor accumsan hendrerit. Suspendisse potenti. Mauris tristique sagittis ante. Nam tempus nibh vel pretium maximus.'
  },
  {
    q: 'What is the benefit of Red Alert compared to other home invasion alarms?',
    a: 'Fusce risus enim, pulvinar eu vestibulum id, semper nec libero. Curabitur condimentum sem vel euismod molestie. Etiam rutrum massa et tortor accumsan hendrerit. Suspendisse potenti. Mauris tristique sagittis ante. Nam tempus nibh vel pretium maximus. Pellentesque mollis, quam non pulvinar tincidunt, neque eros faucibus dui, in mollis ipsum metus ac nunc.'
  },
  {
    q: 'What guarantees does Red Alert give against burglary?',
    a: 'Mauris at tellus eros. Pellentesque scelerisque turpis blandit enim tristique volutpat. Aliquam quis diam a sem sagittis efficitur vel nec erat. Maecenas vestibulum id dolor eu pharetra. Morbi convallis lacus id fringilla maximus. Nunc condimentum dolor sollicitudin lectus mollis ornare.'
  },
  {
    q: 'Why is Red Alert so cheap compared to other competitors?',
    a: 'Cras condimentum lacus ut enim lobortis commodo. Integer faucibus metus eget odio facilisis tincidunt. Morbi vestibulum tristique felis, ut convallis odio mattis sit amet.'
  }
];

const _onItemClick = (index, active, setActive) => () => {
  if (index === active) {
    setActive(null);

    return;
  }

  setActive(index);
};

const _renderItems = (active, setActive) => (item, index) => {
  const { q, a } = item;
  const selected = index === active;
  const lastIndex = index === questionList.length - 1;

  return (
    <div
      key={q}
      className="faqItem"
      role="button"
      tabIndex="0"
      data-testid={`faq-${index}`}
      onClick={_onItemClick(index, active, setActive)}
      onKeyPress={_onItemClick(index, active, setActive)}
      focusable
    >
      <div className="faqItemHeader" data-selected={selected} data-last={lastIndex}>
        <span>{q}</span>
        <span className="arrow" />
      </div>
      <p className="faqItemAnswer" data-selected={selected} data-last={lastIndex}>{a}</p>
    </div>
  );
};

const FAQ = () => {
  const [active, setActive] = React.useState(null);

  return (
    <div className="faqSection">
      <h5>Most asked questions</h5>
      <div className="faqContainer">
        {questionList.map(_renderItems(active, setActive))}
      </div>
    </div>
  );
};

export default FAQ;
