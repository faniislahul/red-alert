import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import FAQ from './FAQ.component';

jest.useFakeTimers();

describe('FAQ Section Component test', () => {
  it('should return correct snapshot ', () => {
    const { container } = render(<FAQ />);

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should set FAQ item #1 to active when clicked', () => {
    const mockState = null;
    const mockSetState = jest.fn();
    jest.spyOn(React, 'useState').mockReturnValue([mockState, mockSetState]);
    const { getByTestId } = render(<FAQ />);
    const component = getByTestId('faq-0');

    fireEvent.click(component);

    expect(mockSetState).toBeCalledWith(0);
  });

  it('should set active item to null when current item clicked', () => {
    const mockState = 0;
    const mockSetState = jest.fn();
    jest.spyOn(React, 'useState').mockReturnValue([mockState, mockSetState]);
    const { getByTestId } = render(<FAQ />);
    const component = getByTestId('faq-0');

    fireEvent.click(component);

    expect(mockSetState).toBeCalledWith(null);
  });
});
