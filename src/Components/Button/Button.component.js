import React from 'react';
import PropTypes from 'prop-types';

import './Button.component.styles.scss';

const Button = ({
  variant, title, onClick, id
}) => (
  <button
    className="button"
    type="button"
    id={id}
    data-testid={id}
    data-variant={variant}
    onClick={onClick}
    tabIndex="0"
    focusable
  >
    <span>{title}</span>
  </button>
);

Button.propTypes = {
  id: PropTypes.string,
  variant: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired
};

Button.defaultProps = {
  id: null
};

export default Button;
