import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import Button from './Button.component';

jest.useFakeTimers();

describe('Button Component test', () => {
  const props = {
    variant: 'red',
    onClick: jest.fn(),
    title: 'Button',
    id: 'Button'
  };

  it('should return correct snapshot for red variant', () => {
    const { container } = render(<Button {...props} />);

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should return correct snapshot for black variant', () => {
    const mockProps = {
      ...props,
      variant: 'black'
    };

    const { container } = render(<Button {...mockProps} />);

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should call onClick function when clicked', () => {
    const { getByTestId } = render(<Button {...props} />);
    const component = getByTestId('Button');

    fireEvent.click(component);

    expect(props.onClick).toBeCalled();
  });
});
